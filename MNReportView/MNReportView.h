/*
 author:陈庆明
 email:282255651@qq.com
 
 辅助说明：
    1.为配合服务端传递的数据MNReportData和MNReportGrid设置的颜色都为NSString类型，非UIColor，颜色字符串格式：(1).六位数字(例:887788) (2).0x887788 （3）#FF778877(截取后六位) 若不适配可以自行修改.m中的colorFromHexString方法。
 
 补充说明:
    1.支持IOS5.0以上。
    2.使用ARC
    3.未进行详尽测试，某些功能混合使用时可能存在部分bug。
 
 遗留问题:
    1.下划线还是有些问题（当文字中出现一些类似于冒号空格这类占格比较小的字符时候，中文占fontSize,英文占fontSize/2, 但是":","-","空格"之类的占格小于fontSize/2且不规律）
    2.自适应行高超过4行后会出现上下空隙太大的情况，不影响使用(基本不会超过四行)暂时不予处理
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class MNReportView;

#pragma mark - kMNReportDefault

#define kMNReportDefaultHeaderRowHeight 30.0    //表头默认行高
#define kMNReportDefaultGridHeight 30.0         //单元格默认高度
#define kMNReportDefaultLeftWidth 77.0          //第一列默认宽度
#define kMNReportDefaultGridWidth 76.0          //单元格默认宽度

#define kMNReportDefaultFontSize 13.0   //默认字体大小

#define kMNReportDefaultHeaderBackgroundColor [UIColor colorWithRed:0.70 green:0.80 blue:0.90 alpha:1] //表头默认背景色
#define kMNReportDefaultBodyBackgroundColor [UIColor whiteColor]                                       //表体默认背景色
#define kMNReportDefaultHeaderTextColor [UIColor whiteColor]                                           //表头默认字体颜色
#define kMNReportDefaultBodyTextColor [UIColor blackColor]                                             //表体默认字体颜色

#define kMNReportDefaultHeaderTextAlignment NSTextAlignmentCenter   //表头默认文字对其方式
#define kMNReportDefaultBodyTextAlignment NSTextAlignmentCenter     //表体默认文字对其方式

#define kMNReportDefaultBorderlineColor [UIColor colorWithRed:160/255.0 green:190/255.0 blue:210/255.0 alpha:1.0] //报表默认线(分割线和边界线)颜色
#define kMNReportDefaultHorizonLineWidth 1.0                                                                      //报表默认水平分割线颜色
#define kMNReportDefaultVerticalLineWidth  1.0                                                                    //报表默认竖直分割线颜色

#define kMNReportDefaultTopBorderLineWidth 1.0      //报表默认上边界线宽度
#define kMNReportDefaultBottomBorderLineWidth 1.0   //报表默认下边界线宽度
#define kMNReportDefaultLeftBorderLineWidth 1.0     //报表默认左边界线宽度
#define kMNReportDefaultRightBorderLineWidth 1.0    //报表默认右边界线宽度

#define kMNReportDefaultWidthSizeFitType MNReportWidthSizeFitTypeAll    //报表报表宽度自适应方式
#define kMNReportDefaultHeightSizeFitType MNReportHeightSizeFitTypeNone //报表报表高度自适应方式

#pragma mark - enum MNReportWidthSizeFitType

//当实际报表宽度小于设定的frame宽度时候，自适应的规则
typedef enum {
    MNReportWidthSizeFitTypeNone = 0,    //不做任何处理，可能留黑边
    MNReportWidthSizeFitTypeAll,         //全部列平分设定宽度，占满frame，可能导致原来比较宽的某列适应后反而变小。
    MNReportWidthSizeFitTypeWithoutLeft, //第一列不动，其它列平分占满。
} MNReportWidthSizeFitType;

#pragma mark - enum MNReportHeightSizeFitType

//当实际报表高度小于设定的frame高度时候，自适应的规则
typedef enum {
    MNReportHeightSizeFitTypeNone = 0,      //不做任何处理，可能留黑边
    MNReportHeightSizeFitTypeAll,           //全部行平分设定高度，占满frame，可能导致表头高度太窄。
    MNReportHeightSizeFitTypeWithoutHeader, //表头高度不变，其它行平分占满
} MNReportHeightSizeFitType;

#pragma mark - MNReportLabel

@interface MNReportLabel : UILabel

@property (nonatomic, readonly) NSInteger row;
@property (nonatomic, readonly) NSInteger col;
@property (nonatomic, readonly) BOOL underline;
@property (nonatomic, readonly) UIImage *image;

@end

#pragma mark - MNReportGrid
/*
 * 单元格
 */
@interface MNReportGrid : NSObject

@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *textColor;
@property (nonatomic, copy) NSString *backgroundColor;
@property (nonatomic, assign) NSTextAlignment textAlignment;
@property (nonatomic, assign) CGFloat fontSize;
@property (nonatomic, assign) BOOL bold;
@property (nonatomic, assign) BOOL underline;
@property (nonatomic, assign) NSInteger colspans;   //单元格跨列，使用情景：有些表头的单元格可能需要跨列。(不支持表体中跨列)
@property (nonatomic, strong) UIImage *image;

@end

#pragma mark - MNReportData
/*
 * 数据源 - 对应一张报表所需的所有数据和格式
 */
@interface MNReportData : NSObject

@property(nonatomic, copy) NSArray *headerDatas;  //表头数据 - MNReportGrid
@property(nonatomic, copy) NSArray *bodyDatas;    //表体数据 - MNReportGrid

@property (nonatomic, assign) CGFloat headerRowHeight;  //表头行高度，表头可能有多行
@property (nonatomic, assign) CGFloat leftWidth;        //第一列宽度
@property (nonatomic, assign) CGFloat gridWidth;        //单元格宽度
@property (nonatomic, assign) CGFloat gridHeight;       //单元格高度

@property (nonatomic, copy) NSArray *colWidthArray;     //各列宽度,用于可配置列宽，各列列宽不一致时候使用 - NSNumber

@property (nonatomic, assign) BOOL autoFitHeaderHeight;   //表头自适应行高,当一行中的某个单元格文字比较多的时候，自动调整行高使文字全部显示。
@property (nonatomic, assign) BOOL autoFitBodyHeight;     //表体自适应行高

@property (nonatomic, copy) NSString *headerBackgroundColor;    //表头背景颜色
@property (nonatomic, copy) NSString *bodyBackgroundColor;      //表体背景颜色

@property (nonatomic, copy) NSString *headerTextColor;   //表头字体颜色
@property (nonatomic, copy) NSString *bodyTextColor;     //表体字体颜色

@property (nonatomic, assign) CGFloat headerFontSize; //表头字体大小
@property (nonatomic, assign) CGFloat bodyFontSize;   //表体字体大小

@property(nonatomic, assign) NSTextAlignment headerTextAlignment;   //表头对齐方式
@property(nonatomic, assign) NSTextAlignment bodyTextAlignment;     //表体对齐方式

@property (nonatomic, copy) NSString *borderlineColor;   //边界线的颜色
@property (nonatomic, assign) CGFloat horizonLineWidth;  //水平线宽度
@property (nonatomic, assign) CGFloat verticalLineWidth; //竖直线宽度

@property (nonatomic, assign) CGFloat topBorderLineWidth;    //上边界线宽度
@property (nonatomic, assign) CGFloat bottomBorderLineWidth;
@property (nonatomic, assign) CGFloat leftBorderLineWidth;
@property (nonatomic, assign) CGFloat rightBorderLineWidth;

@property (nonatomic, assign) MNReportWidthSizeFitType widthSizeFitType;    //当实际表格宽度小于设置的Frame时候调整单元格宽度
@property (nonatomic, assign) MNReportHeightSizeFitType heightSizeFitType;  //当实际表格高度小于设置的Frame时候调整单元格高度

@end

#pragma mark -MNReportViewTouchDelegate
/*
 * 代理，touch事件
 */
@protocol MNReportViewTouchDelegate <NSObject>

@optional

- (void)MNReportView:(MNReportView*)table clickInLabel:(MNReportLabel *)label;
- (void)MNReportView:(MNReportView*)table longPressInLabel:(MNReportLabel *)label;

@end

#pragma mark -MNReportView
/*
 * 排序相关
 */
typedef enum {
    MNReportSortTypeASC = -1,   //递增
    MNReportSortTypeDSC = 1     //递减
} MNReportSortType;

@interface MNReportSort : NSObject

@property (nonatomic, readonly) NSInteger index;          //排序的列
@property (nonatomic, readonly) MNReportSortType type;    //排序方式
@property (nonatomic, readonly) NSArray *sortedArray;     //排序依赖的顺序数组

- (instancetype)initWithSortedArray:(NSArray *)sortedArray type:(MNReportSortType)type index:(NSInteger)index;

@end
/*
 * 主代码
 */
@interface MNReportView : UIView <UIScrollViewDelegate>

@property (nonatomic, strong) MNReportData *dataSource;             //列表数据源
@property (nonatomic, weak) id<MNReportViewTouchDelegate> delegate; //触摸事件代理

@property (nonatomic, readonly) CGFloat height;
@property (nonatomic, readonly) MNReportSort *currentSort;

- (id)initWithFrame:(CGRect)rect dataSource:(MNReportData *)dataSource; //init要在主线程中执行。
- (void)reload; //由于有比较多涉及到UI，所以reload操作要在主线程中执行，避免出现问题。

- (void)sortAtIndex:(NSInteger)index;
- (void)addSort:(MNReportSort *)sort;   //添加某一列的排序规则，重复添加会替换掉

+ (CGFloat)heightForData:(MNReportData *)data;    //计算datasource所产生表格高度
+ (UIColor *)colorFromHexString:(NSString *)hexString;

@end
