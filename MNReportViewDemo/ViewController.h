//
//  ViewController.h
//  MNReprotDemo
//
//  Created by Chenly on 13-12-26.
//  Copyright (c) 2013年 Chenly. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MNReportView.h"

@interface ViewController : UIViewController <UITabBarDelegate, MNReportViewTouchDelegate>

@end
